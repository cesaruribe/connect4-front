import React from "react";
import { Card, Statistic } from "antd";

const Result = ({ Winner }) => {
  return (
    <div style={{ padding: "30px" }}>
      <Card>
        <Statistic
          title="Winner"
          value={Winner}
          valueStyle={{ color: "#014f86", fontSize: 20 }}
        ></Statistic>
      </Card>
    </div>
  );
};

export default Result;
