import React from "react";
import "../App.css";

const AppTitle = () => {
  return (
    <div className="App-header">
      <h1 style={{ paddingTop: 10, fontSize: 40, color: "#eee" }}>
        CONNECT 4 GAME
      </h1>
    </div>
  );
};

export default AppTitle;
