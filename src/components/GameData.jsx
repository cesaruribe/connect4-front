import { React, useState, useEffect } from "react";
import { Table, Button } from "antd";
import { Link } from "react-router-dom";
import Title from "./Title";

const GameData = () => {
  const [data, setData] = useState([]);

  const columns = [
    {
      title: "ID",
      dataIndex: "Id",
      key: "Id",
    },
    {
      title: "Game uuid",
      dataIndex: "GameUUID",
      key: "GameUUID",
    },
    {
      title: "ID",
      dataIndex: "Id",
      key: "Id",
    },
    {
      title: "Winner Id",
      dataIndex: "Winner",
      key: "Winner",
    },
    {
      title: "Game Tied",
      dataIndex: "Tied",
      key: "Tied",
      render: (text, value, index) => (!value ? "YES" : "NO"),
    },
    {
      title: "Total Movements",
      dataIndex: "TotalMoves",
      key: "TotalMoves",
    },
    {
      title: "Movements",
      dataIndex: "Movements",
      key: "Movements",
    },
    {
      title: "Date",
      dataIndex: "Date",
      key: "Date",
    },
  ];

  useEffect(() => {
    fetch("http://localhost:8000/games/")
      .then((data) => data.json())
      .then((items) => {
        let newData = items.map((item) => ({
          ...item,
          Movements: JSON.stringify(JSON.parse(item.Movements).data),
        }));
        setData(newData);
      });
  }, []);

  return (
    <div>
      <Title></Title>
      <h1 style={{ margin: "50px 0 " }}>All Results</h1>
      <div style={{ padding: "5%" }}>
        <Table
          columns={columns}
          dataSource={data}
          pagination={{ pageSize: 10 }}
          scroll={{ x: 1000, y: 240 }}
        ></Table>
      </div>
      <Link to="/">
        <Button type="primary" style={{ marginLeft: 8 }}>
          Main Page
        </Button>
      </Link>
      <Link to="/">
        <Button type="primary" style={{ marginLeft: 8 }}>
          Play
        </Button>
      </Link>
    </div>
  );
};

export default GameData;
