import React from "react";
import { Descriptions, Badge } from "antd";
import "../App.css";

const DataBoard = ({
  Status,
  Turn,
  ID,
  Color,
  ColorName,
  Name,
  SID,
  Nusers,
}) => {
  return (
    <div style={{ backgroundColor: "#ffffff", margin: "0px 50px 30px 50px" }}>
      <Descriptions
        title="Board Information"
        bordered
        size="small"
        column={1}
        labelStyle={{
          backgroundColor: "#dddddd",
          fontSize: 17,
          fontWeight: 800,
          width: "50%",
        }}
        contentStyle={{
          fontSize: 14,
        }}
      >
        <Descriptions.Item label="Your Name"> {Name} </Descriptions.Item>
        <Descriptions.Item label="Your ID"> {ID} </Descriptions.Item>
        <Descriptions.Item label="Your SID"> {SID} </Descriptions.Item>
        <Descriptions.Item label="Your Color">
          <Badge color={Color} text={ColorName} className="color-player" />
        </Descriptions.Item>
        <Descriptions.Item label="Turn">{`Player ${Turn}`}</Descriptions.Item>
        <Descriptions.Item label="Status">
          <Badge
            status={Status === "Active" ? "processing" : "default"}
            text={Status}
          />
        </Descriptions.Item>
        <Descriptions.Item label="Players connected">
          {Nusers}
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};

export default DataBoard;
