import { React, useState, useRef } from "react";
import { message } from "antd";
import "../App.css";

const cellWidth = 70;
const cellHeight = 70;
const xoffset = 0;
const yoffset = 0;
const radiusFactor = 0.8;

const GameBoard = ({
  board,
  sizeX,
  sizeY,
  socket,
  turn,
  id,
  winner,
  NUser,
}) => {
  //const [isHover, setIsHover] = useState(false);
  const [sside, setSide] = useState("");
  let isHover = useRef(false);
  // let side = useRef("");
  const [srow, setSRow] = useState("");

  const cells = [];

  const isYourTurn = id === turn ? true : false;

  const getClick = (ev, socket, row) => {
    if (winner) {
      message.error({
        content:
          "Remember: you must reset the board to play again and wait for other player",
        duration: 2,
        style: { padding: "10px", fontSize: "20px" },
      });
      return;
    }

    if (NUser < 2) {
      message.warning({
        content: "Please, you must wait for other player to begin the game",
        duration: 2,
        style: { padding: "10px", fontSize: "20px" },
      });
      return;
    }

    const side =
      ev.nativeEvent.offsetX < 0.5 * cellWidth * sizeX ? "left" : "right";

    if (isYourTurn && NUser > 1) {
      socket.emit("play", [row, side]);
    } else {
      console.log("Else: ", isYourTurn, NUser);
      if (NUser > 1) {
        message.warning({
          content: "Sorry but it's not your turn",
          duration: 1,
          style: { padding: "10px", fontSize: "20px" },
        });
      } else {
        message.warning({
          content: "Sorry but you must way for the another player",
          duration: 1,
          style: { padding: "10px", fontSize: "20px" },
        });
      }
    }
  };

  // Get side and row of board, selected by the pointer
  const handleHover = (ev, row) => {
    const pointer_side =
      ev.nativeEvent.offsetX < 0.5 * cellWidth * sizeX ? "left" : "right";
    setSide(pointer_side);
    // side.current = pointer_side;
    setSRow(row);
    // console.log(row, side.current);
  };

  // Re-Rendering the board to fill the circle
  // where could be the next play depending of
  // ponter location
  const Fill = (row, col, board) => {
    let column = "";
    if (sside === "left") {
      // if (side.current === "left") {
      for (let j = 0; j < 7; j++) {
        if (board[row][j] === "#fdfffc") {
          column = j;
          break;
        }
      }
    }
    if (sside === "right") {
      // if (side.current === "right") {
      for (let j = 6; (j) => 0; j--) {
        if (board[row][j] === "#fdfffc") {
          column = j;
          break;
        }
      }
    }

    // if (isHover && column === col && row === srow) {
    if (isHover.current && column === col && row === srow) {
      let instyle = "#03ff6a";
      return instyle;
    } else {
      return board[row][col];
    }
  };

  for (let i = 0; i < sizeX; i++) {
    for (let j = 0; j < sizeY; j++) {
      let cell = (
        <g
          key={i * sizeX + j}
          onClick={(ev) => getClick(ev, socket, i)}
          onMouseOver={(ev) => handleHover(ev, i, j, board)}
          onMouseEnter={() => {
            //setIsHover(true);
            isHover.current = true;
          }}
          onMouseLeave={() => {
            isHover.current = false;
            //setIsHover(false);
          }}
        >
          <rect
            x={j * cellWidth + xoffset}
            y={i * cellHeight + yoffset}
            width={cellWidth}
            height={cellHeight}
            fill={"#8f2d56"}
          />
          <circle
            id={`${i}${j}`}
            cx={j * cellWidth + 0.5 * cellWidth + xoffset}
            cy={i * cellHeight + 0.5 * cellHeight + yoffset}
            r={0.5 * cellWidth * radiusFactor}
            fill={Fill(i, j, board)} //{board[i][j]}
          />
        </g>
      );
      cells.push(cell);
    }
  }

  const divStyle = {
    display: "Flex",
    justifyContent: "center",
  };

  return (
    <div style={divStyle}>
      <svg width={cellWidth * sizeX} height={cellHeight * sizeY}>
        {cells}
      </svg>
    </div>
  );
};

export default GameBoard;
