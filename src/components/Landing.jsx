import React from "react";
import { Button, Space } from "antd";
import { Link } from "react-router-dom";
import "../App.css";

const Landing = () => {
  return (
    <div>
      <div className="Landing-component">
        <div className="landing-content">
          <h1 style={{ color: "white" }}>Welcome to Connect 4 game</h1>
          <Space size="large">
            <Link to="/board">
              <Button type="primary" shape="round" size="large">
                Start Game
              </Button>
            </Link>
            <Link to="/allgames">
              <Button type="primary" shape="round" size="large">
                View all results
              </Button>
            </Link>
          </Space>
        </div>
      </div>
    </div>
  );
};

export default Landing;
