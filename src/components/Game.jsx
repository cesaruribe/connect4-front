import React, { useState, useEffect, useRef } from "react";
import socketIOClient from "socket.io-client";
import { Button, Row, Col, Divider, Alert, notification } from "antd";
import "antd/dist/antd.css";
import DataBoard from "./DataBoard";
import GameBoard from "./GameBoard";
import Result from "./Result";
import AppTitle from "./Title";
import { Link } from "react-router-dom";
import "../App.css";

const ENDPOINT = `http://${process.env.REACT_APP_URL_SERVER}:${process.env.REACT_APP_PORT}`;
console.log(process.env.REACT_APP_URL_SERVER);

const Game = () => {
  const nrows = 7;
  const ncols = 7;

  const [myTurn, setMyTurn] = useState(1);
  const [idsocket, setIdsocket] = useState("");
  const [color, setColor] = useState("");
  const [name, setName] = useState("");
  const [colorName, setColorName] = useState("");
  const [id, setId] = useState();
  const [winner, setWinner] = useState(false);
  const [winnerName, setWinnerName] = useState(" ");
  const [board, setBoard] = useState(
    Array(nrows)
      .fill(0)
      .map((x) => Array(ncols).fill("#fdfffc"))
  );
  const [status, setStatus] = useState("Inactive");
  const [nuser, setNUsers] = useState(0);
  const [connected, setConnected] = useState();
  const [show, setShow] = useState(false);

  const ws = useRef(null);
  const turno = useRef(myTurn);

  useEffect(() => {
    ws.current = socketIOClient(ENDPOINT, { path: "/ws/socket.io" });

    ws.current.on("connect", () => {
      setIdsocket(ws.current.id);
      setStatus("Active");
      setConnected(true);
      setShow(true);
      console.log("CONNECTED");
    });

    ws.current.on("disconnect", () => {
      console.log("DISCONNECTED");
      setNUsers(1);
      setStatus("Inactive");
    });

    return () => ws.current.disconnect();
  }, []);

  useEffect(() => {
    if (!ws.current) return;

    // Receive message on room "board"
    ws.current.on("board", (data) => {
      let localBoard = JSON.parse(data);
      setBoard(localBoard);
    });

    ws.current.on("set_player", (data) => {
      try {
        data = JSON.parse(data);
        setName(data.name);
        setColor(data.color);
        setColorName(data.colorname);
        setId(data.id);
      } catch (err) {
        console.log("not server yet");
      }
    });

    ws.current.on("turn", (data) => {
      setMyTurn(data);
      turno.current = data;
    });

    ws.current.on("winner", (data) => {
      setWinner(true);
      setWinnerName(`Player ${data}`);
    });

    ws.current.on("connections", (data) => {
      setNUsers(data);
    });

    ws.current.on("reset_client", (data) => {
      setBoard(
        Array(nrows)
          .fill(0)
          .map((x) => Array(ncols).fill("#fdfffc"))
      );
      setWinner(false);
      setWinnerName(" ");
      setMyTurn(1);
    });
  }, []);

  useEffect(() => {
    if (!ws.current) return;
    ws.current.on("connect_error", (err) => {
      try {
        err = JSON.parse(err.message);

        if (err.type === "filled") {
          console.log("connect_error was....: ", err.message, err.data);
          setConnected(err.data);
          setShow(true);
        }
      } catch (error) {
        console.log(error);
      }
    });
  }, [show]);

  const resetGame = () => {
    setBoard(
      Array(nrows)
        .fill(0)
        .map((x) => Array(ncols).fill("#fdfffc"))
    );
    setWinner(false);
    setWinnerName(" ");
    setMyTurn(1);
    ws.current.emit("reset_server");
  };

  if (connected && show) {
    notification.success({
      message: "Connected",
      duration: 2,
      description:
        "You can play but wait for other player if you are the  only one",
    });
    setShow(false);
  }
  if (!connected && show) {
    notification.error({
      message: "Connection Error",
      duration: 2,
      description: "You cannot connect because there is already 2 players",
    });
    setShow(false);
  }

  return (
    <div className="App">
      <AppTitle />

      <div style={{ paddingTop: 30 }}>
        <Row gutter={[20, 24]}>
          <Col flex={1}>
            <div style={{ margin: "0px auto" }}>
              <Row>
                <Col span={24}>
                  <DataBoard
                    Color={color}
                    ColorName={colorName}
                    ID={id}
                    Turn={myTurn}
                    Name={name}
                    Nusers={nuser}
                    Status={status}
                    SID={idsocket}
                  />
                </Col>
              </Row>

              <Row justify="center" align="middle">
                <Col span={12}>
                  <Result Winner={winnerName} />
                </Col>

                {winner && (
                  <Col span={12}>
                    <Alert
                      message="Game over"
                      description="Someone of player must reset the board"
                      type="info"
                      showIcon
                    />
                  </Col>
                )}
              </Row>
            </div>
          </Col>

          <Col span={1}>
            <Divider type="vertical" style={{ height: "100%" }} />
          </Col>

          <Col flex={5}>
            <GameBoard
              board={board}
              sizeX={7}
              sizeY={7}
              socket={ws.current}
              turn={myTurn}
              id={id}
              winner={winner}
              NUser={nuser}
            />
          </Col>
        </Row>
      </div>

      <Divider></Divider>
      <div style={{ marginTop: "50px" }}>
        <Button type="primary" onClick={resetGame} style={{ marginLeft: 8 }}>
          Reset game
        </Button>
        <Button
          type="primary"
          onClick={() => ws.current.connect()}
          style={{ marginLeft: 8 }}
        >
          Connect
        </Button>
        <Button
          type="primary"
          onClick={() => ws.current.disconnect()}
          style={{ marginLeft: 8 }}
        >
          Disconnect
        </Button>
        <Link to="/">
          <Button
            type="primary"
            onClick={() => ws.current.disconnect()}
            style={{ marginLeft: 8 }}
          >
            Logout
          </Button>
        </Link>
      </div>
    </div>
  );
};

export default Game;
