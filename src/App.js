import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "antd/dist/antd.css";
import Game from "./components/Game";
import Landing from "./components/Landing";
import GameData from "./components/GameData";
function App() {

  return (
    <div className='App'>
      <Router>
        <Switch>
          <Route exact path="/board" component={Game}/>
          <Route path='/allgames' component={GameData}/>
          <Route exact path="/" component={Landing}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
