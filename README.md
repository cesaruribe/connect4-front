# CONNECT 4 GAME

This is an improved connect 4 game where additional rules for play were applied. Basically, this game consists of 2 players who must put game tokens of two different colors (each one color corresponds to a player) into a column. These tokens are being stacked and the end of a column and the player who get align or connect 4 of your tokens will win

![General image](./img/connect-4-rules.jpg)
Image obtained from this [link](http://kierangamedesign.blogspot.com/2018/04/connect-4-one-of-greatest-board-games.html)

In this case, additional rules were added:

- Tokens are stacked horizontally and not vertically
- Players can play or stack pieces for both sides: left and right. If the player selects the left side of a row, the pieces are stacked for this side.

## Technical description

In order to create a web app that allows connect 2 players online and play the game, this was split into 2 components:

- Client - Frontend side
- Server - Backend side

In this application, **only 2 players can play simultaneously**. If a third player (another tab of your browser for example ) tr to connect, he receives an error message that says to him the board is complete with 2 players. _It could be implemented in the future, a feature that allows to players see different rooms and connect throw them._

Another thing to mention is there are two colors that identify the players as well as an ID:

- Player 1 - sky blue - ID 1
- Player 2 - Yellow - ID 1

Every time the game begins, the initial turn is always assigned to the first connected player (Player 1), and the board pops up an alert when a player tries to play when it is not his turn.

Next up you can read the technical details of each one of these components:

## FrontEnd Component

This component was developed using the React JS library and the tool create-react-app. It was implemented with 3 views where a player can connect, play and see the result of all the games played

**Landing View**

![landing](./img/landing.png)

**Data Game View**

![game data](./img/game-results.png)

**Game Board**

![board](./img/board.png)

**Game played**

![played](./img/game-played.png)

In order for players could play in real time, the sytem was constructed using websockets wih socket.io library for frontend and backend. However, there are others alternatives like grpc or graphql which could be implemented. This could be a future revision

### installation and execution

To install all libraries, you must have `npm` installed in your pc. To install the dependencies, yu just must run the following command: `npm install`

To run locally and develop mode, you can execute `npm start`

You need to change the .env file where the value of URL_SERVER and PORT is defined. It is necessary because you will have a different environment and the client needs to know what URL or port it must do the requests.

## Backend Component

The backend component was developed using the [FastAPI](https://fastapi.tiangolo.com/) library and [python-socketio](https://python-socketio.readthedocs.io/en/latest/index.html) to create a server that receives or communicate both HTTP and WebSocket.

The backend system has two components:

- Websocket service: to communicate and connect players
- HTTP server to query data saved in a database
- Database layer using SQlite by easy use and transport information. Tihs database store the information of games played.

To run the server you can use several tools like pyenv to create virtual environments, pipenv to install dependencies in virtual environment or poetry htat do the same as pipenv. Once you had installed all dependencies (these dependencies can be found in the Pipfile or myproject.toml files) you can execute the server running the following command:

```
uvicorn server:app --host 0.0.0.0 --reload
```

or

```
poetry run uvicorn server:app --host 0.0.0.0 --reload
```
